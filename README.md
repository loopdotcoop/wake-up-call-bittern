# Wake Up Call (Bittern)

#### An abstract 3D animation with endangered birds

[Home](https://loopdotcoop.gitlab.io/wake-up-call-bittern)
[Repo](https://gitlab.com/loopdotcoop/wake-up-call-bittern)

Art by [Beth Walker.](http://filmsbybeth.com/)  
Code by [Rich Plastow.](http://richplastow.com/)  
[Music](https://cosmosheldrake.bandcamp.com/track/wake-up-call-bittern) by
[Cosmo Sheldrake.](http://www.cosmosheldrake.com/)

Featuring a Bittern, a Short Eared Owl, a Cuckoo, a Black Throated Diver, a
Skylark, a Lapwing, a Nightingale, a Snow Bunting, a Linnet, a Quail, a Pied
Flycatcher and a recording of a dawn chorus in Dorset.
