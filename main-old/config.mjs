//// Configuration.

export default {
    defaults: {
        mainWidth: 512
      , asideWidth: 482

        //// Timeline.
      , isPlaying: true
      , playtime: 0
      , duration: 5000 // in milliseconds

        //// Lights.
      , singlehue: 180
      , singlelightness: 50
      , singleshow: false // whether to show position of the single-light
      , pairhue: 60
      , pairlightness: 50
      , pairshow: false

        //// Position.
      , xposition: 0
      , yposition: 0
      , zposition: 0

    }

  , capture: {
        fps: 25
      , quality: 'ultrafast' // ultrafast|superfast|veryfast|faster|fast|medium|slow|slower|veryslow|placebo
    }

  , minMainWidth: 512
  , minAsideWidth: 482
  , durationMin: 500
  , durationMax: 30000
  , path: 'asset/svg/undo3d-logos-v1.svg'
  , pixelRatio: window.devicePixelRatio || 0
}
