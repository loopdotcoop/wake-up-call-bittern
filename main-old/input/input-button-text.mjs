//// Text Button.
import InputButton from './input-button.mjs'

export default class InputButtonText extends InputButton {
    constructor (options) {
        super(options)
        this.$el.innerHTML = options.title
    }
}
