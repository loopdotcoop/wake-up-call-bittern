//// Lightswitch.
import Input from './input.mjs'

export default class InputLightswitch extends Input {
    constructor (options) {
        super(options)
        this.$el.addEventListener('click', options.onClick)
        this.titleSuffix = options


        //// Add a single light-icon...
        if (! options.isPair) {
            const $lighticon = this.$lighticon = document.createElement('span')
            $lighticon.id = this.$el.id + '-icon' // eg 'singlelightswitch-icon'
            $lighticon.classList.add('single')
            this.$el.appendChild($lighticon)

        //// ...or add a pair of light-icons.
        } else {
            const $lighticon1 = this.$lighticon1 = document.createElement('span')
            const $lighticon2 = this.$lighticon2 = document.createElement('span')
            $lighticon1.id = this.$el.id + '-icon1' // eg 'pairlightswitch-icon1'
            $lighticon2.id = this.$el.id + '-icon2' // eg 'pairlightswitch-icon1'
            $lighticon1.classList.add('pair','pair-1')
            $lighticon2.classList.add('pair','pair-2')
            this.$el.appendChild($lighticon1)
            this.$el.appendChild($lighticon2)
        }

    }

    get tagName () { return 'button' }
    get className () { return 'lightswitch btn' }

        // .lightswitch { position:absolute; margin:-40px 0 0 -40px; cursor:pointer; }
        // .lightswitch { width:32px; height:32px; border-radius:16px; border:none; }

}
