//// A generic list of items.

import InputButtonSvg from '../input/input-button-svg.mjs'
import InputButtonText from '../input/input-button-text.mjs'

export default class List {

    constructor (options) {
        this.selector = options.selector
        this.$el = document.querySelector(this.selector)
        if (! this.$el) throw Error(`No such element '${this.selector}'`)
    }

    addButton (options) {
        if (options.svg) // has an SVG image
            this.addEl(InputButtonSvg, options)
        else // no image, just make a text-button
            this.addEl(InputButtonText, options)
    }

    addEl (Input, options) {
        const id = '$'+options.id
        this[id] = new Input(options)
        this.$el.appendChild(this[id].$el)
    }

}
