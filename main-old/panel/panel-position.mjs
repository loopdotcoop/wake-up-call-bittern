//// A control panel for the scene’s x, y and z position.

import Panel from './panel.mjs'
import state from '../state.mjs'

export default class PanelPosition extends Panel {

    constructor (options) {
        super(options)

        this.scene = options.scene

        state.panel.position = { }

        const isAside = !! document.querySelector('aside ' + this.selector)




        this.addSlider({
            id: 'xposition'
          , panelState: state.panel.position
          , isAside
          , min: -5120
          , max: 0
          , labelRounder: v => ~~v
          , labelUnit: ''
          , onSetValue: v => {
                state.xposition = ~~v
                this.updatePosition()
            }
        })


        this.addSlider({
            id: 'yposition'
          , panelState: state.panel.position
          , isAside
          , min: -512
          , max: 512
          , labelRounder: v => ~~v
          , labelUnit: ''
          , onSetValue: v => {
                state.yposition = ~~v
                this.updatePosition()
            }
        })


        this.addSlider({
            id: 'zposition'
          , panelState: state.panel.position
          , isAside
          , min: -10000
          , max: 500
          , labelRounder: v => ~~v
          , labelUnit: ''
          , onSetValue: v => {
                state.zposition = ~~v
                this.updatePosition()
            }
        })

    }

    updatePosition () {
        this.scene.svg.mesh.position.set(
            state.xposition || 0
          , state.yposition || 0
          , state.zposition || 0
        )
        // this.scene.camera.lookAt(state.xposition+256, state.yposition-256, 0)
    }

}
