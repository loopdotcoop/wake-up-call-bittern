//// A control panel for the animation’s timeline.

import Panel from './panel.mjs'
import config from '../config.mjs'
import state from '../state.mjs'

export default class PanelTimeline extends Panel {

    constructor (options) {
        super(options)

        state.panel.timeline = { }
        state.panel.duration = { }

        this.addSlider({
            id: 'playtime'
          , panelState: state.panel.timeline
          , isAside: !! document.querySelector('aside ' + this.selector)
          , min: 0
          , max: state.duration //@TODO make this a getter
          , labelRounder: v => ~~v
          , labelUnit: ''
          , onPreciseBegin: () => state.isPlaying = false
          , onSetValue: v => state.playtime = ~~v
        })

        this.addSlider({
            id: 'duration'
          , panelState: state.panel.duration
          , isAside: !! document.querySelector('aside ' + this.selector)
          , min: config.durationMin
          , max: config.durationMax
          , labelRounder: v => ~~v
          , labelUnit: ''
          , onSetValue: v => {
                this.$playtime.max = state.duration = ~~v
                this.$playtime.thumbState.value += 0 // playtime should never show an out-of-range value
            }
        })


        //// Spacebar toggles playback.
        window.addEventListener('keydown', evt => {
            if (' ' !== evt.key) return
            evt.preventDefault() // eg don’t scroll down by the window-height
            state.isPlaying = ! state.isPlaying
        })


        this.addButton({
            id: 'restart'
          , title: 'Restart'
          , onClick: evt => this.$playtime.thumbState.value = 0
          , size: 512 // width and height of SVG
          , svg: (innerHTML => innerHTML = `
<polygon points="96,256.5 415.998,96 416,416"/>
<rect x="96" y="96" width="96" height="320"/>
            `)()
        })

        this.addButton({
            id: 'play'
          , title: 'Play'
          , onClick: evt => state.isPlaying = true
          , size: 512
          , svg: (innerHTML => innerHTML = `
<polygon points="448,256.5 128.001,96 128,416"/>
            `)()
        })

        this.addButton({
            id: 'pause'
          , title: 'Pause'
          , onClick: evt => state.isPlaying = false
          , size: 512
          , svg: (innerHTML => innerHTML = `
<rect x="128" y="96" width="96" height="320"/>
<rect x="288" y="96" width="96" height="320"/>
            `)()
        })

    }

}
