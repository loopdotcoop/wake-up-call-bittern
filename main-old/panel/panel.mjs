//// A generic control panel.

import InputButtonSvg from '../input/input-button-svg.mjs'
import InputButtonText from '../input/input-button-text.mjs'
import InputSlider from '../input/input-slider.mjs'
import InputLightswitch from '../input/input-lightswitch.mjs'

export default class Panel {

    constructor (options) {
        this.selector = options.selector
        this.$el = document.querySelector(this.selector)
    }

    addSlider (options) {
        this.addEl(InputSlider, options)
    }

    addButton (options) {
        if (options.svg) // has an SVG image
            this.addEl(InputButtonSvg, options)
        else // no image, just make a text-button
            this.addEl(InputButtonText, options)
    }

    addLightswitch (options) {
        this.addEl(InputLightswitch, options)
    }

    addEl (Input, options) {
        const id = '$'+options.id
        this[id] = new Input(options)
        this.$el.appendChild(this[id].$el)
    }

}
