//// Sets of the THREE.js scene.

import config from './config.mjs'
import state from './state.mjs'

export default class Scene {

    constructor (options) {

        this.svg = options.svg
        this.panels = options.panels
        this.prevNow = null

        //// Scene.
        this.scene = new THREE.Scene()

        //// Camera.
        this.camera = new THREE.PerspectiveCamera(
            7.5, state.mainWidth/state.mainWidth, 0.1, 15000)
        this.camera.position.set(256, -256, 4000)
        this.camera.lookAt(256, -256, 0)
        this.scene.add(this.camera)

        //// Renderer.
        const canvas = document.querySelector('#render')
        this.renderer = new THREE.WebGLRenderer({
            canvas
          , antialias:true
          // , preserveDrawingBuffer: true
        })
    	this.renderer.setPixelRatio(config.pixelRatio)
    	this.renderer.autoClear = false
        this.renderer.setSize(state.mainWidth, state.mainWidth)

        //// Composer and copy pass.
        this.composer = new THREE.EffectComposer(this.renderer)
        this.composer.setSize(
            state.mainWidth * config.pixelRatio
          , state.mainWidth * config.pixelRatio
        )
        this.copyPass = new THREE.ShaderPass(THREE.CopyShader)
        this.copyPass.renderToScreen = true
    	this.composer.addPass( new THREE.RenderPass(this.scene, this.camera) )
    	this.composer.addPass(this.copyPass)

        //// Ambient light.
        this.ambientLight = new THREE.AmbientLight(0xFFFFFF, 0.4)
	    this.scene.add(this.ambientLight)

        //// A single point light.
        this.lightBoomSingle = new THREE.Object3D()
	    this.lightBoomSingle.position.set(256, -256, 100)
	    this.scene.add(this.lightBoomSingle)

        this.pointLightSingle = new THREE.PointLight(0xFFCCCC, 2)
	    this.pointLightSingle.position.set(50, -240, -100)
        this.lightBulbSingleMaterial = new THREE.MeshBasicMaterial({ color:0xFFCCCC })
        this.lightBulbSingle = new THREE.Mesh(
            new THREE.SphereGeometry(7, 32, 32), this.lightBulbSingleMaterial )
	    this.pointLightSingle.add(this.lightBulbSingle)
	    this.lightBoomSingle.add(this.pointLightSingle)

        //// A pair of point lights.
        this.lightBoomPair = new THREE.Object3D()
	    this.lightBoomPair.position.set(220, -265, 110)
	    this.scene.add(this.lightBoomPair)

        this.pointLightPair1 = new THREE.PointLight(0x0080FF, 1)
	    this.pointLightPair1.position.set(220, 40, 0)
        this.lightBulbPair1Material = new THREE.MeshBasicMaterial({ color:0x0080FF })
        this.lightBulbPair1 = new THREE.Mesh(
            new THREE.SphereGeometry(5, 32, 32), this.lightBulbPair1Material )
	    this.pointLightPair1.add(this.lightBulbPair1)
	    this.lightBoomPair.add(this.pointLightPair1)

        this.pointLightPair2 = new THREE.PointLight(0x0080FF, 1)
	    this.pointLightPair2.position.set(220, -40, 0)
        this.lightBulbPair2Material = new THREE.MeshBasicMaterial({ color:0x0080FF })
        this.lightBulbPair2 = new THREE.Mesh(
            new THREE.SphereGeometry(5, 32, 32), this.lightBulbPair2Material )
	    this.pointLightPair2.add(this.lightBulbPair2)
	    this.lightBoomPair.add(this.pointLightPair2)

        //// SVG.
        this.scene.add(this.svg.mesh) // position will be updated by PanelPosition

        //// MP4 capture.

        //// Capture.
        this.capture = new THREEcap({
            width: state.mainWidth  // THREEcap default: 320
          , height: state.mainWidth // THREEcap default: 240
          , fps: config.capture.fps // THREEcap default: 25
          , time: state.duration / 1000 // convert ms to seconds
          , format: 'mp4'
          , quality: 'ultrafast' // THREEcap default: 'ultrafast', but also superfast|veryfast|faster|fast|medium|slow|slower|veryslow|placebo
          // , canvas: renderer.domElement // optional, slowest
          , composer: this.composer // faster than using a canvas
          , scriptbase: 'asset/js/threecap/'
        })
        this.captureui = new THREEcapUI(
            this.capture, {
                resolution: '512x512'
              , framerate: config.capture.fps // 25
              , time: state.duration / 1000 // convert ms to seconds
              , format: 'mp4'
              , filename: 'undo3d-logo.mp4'
              , onRecordBegin: () => {
                    const { format, time } = this.captureui.settings
                    console.log(`Recording ${time}s ${format}...`)
                    state.isPlaying = true
                }
              , onEncodeBegin: () => {
                    const { format, time } = this.captureui.settings
                    console.log(`Encoding ${time}s ${format}...`)
                    state.isPlaying = false
                }
              , onReadyToDownload: () => {
                    const { format, time } = this.captureui.settings
                    console.log(`${time}s ${format} ready to download`)
                }
            }
        )

        //// Begin rendering.
        this.boundRender = this.render.bind(this)
        requestAnimationFrame( this.beginRendering.bind(this) )
    }



    beginRendering (now) {
        this.prevNow = now
        for (const $el of document.querySelectorAll('.loading') )
            $el.classList.remove('loading') // eg Slider thumbs
        requestAnimationFrame(this.boundRender)
    }


    render (now) {
        const { $playtime } = this.panels.timeline
        const { playtimeState } = state.panel.timeline
        requestAnimationFrame(this.boundRender)

        //// Get the time diff.
        const diff = now - this.prevNow
        this.prevNow = now

        //// Nudge the playhead forwards unless we’re paused, or the timeline
        //// thumb is being dragged. Loop back to zero if we’re past `duration`.
        if (state.isPlaying && false === playtimeState.startClientX) {
            let newPlaytime = playtimeState.value + diff
            if (newPlaytime > state.duration) newPlaytime -= state.duration
            playtimeState.value = newPlaytime
        }

        //// Update light positions.
        const rads = Math.PI * 2 * state.playtime / state.duration
        this.lightBoomSingle.rotation.z = Math.PI * -2 - rads
        this.lightBoomSingle.rotation.y = Math.PI - rads
        this.lightBoomPair.rotation.z = Math.PI + rads

        //// Apply current lightswitch settings.
        this.lightBulbSingle.visible = state.singleshow
        this.lightBulbPair1.visible = this.lightBulbPair2.visible = state.pairshow

        //// Render the frame.
    	this.renderer.clear()
    	this.composer.render()
        // this.renderer.render(this.scene, this.camera)

        //// Deal with a request to generate a new screengrab. @TODO find a better way
        if (state.mimeForNextFrameDownload) {
            const mime = state.mimeForNextFrameDownload
            state.mimeForNextFrameDownload = false
            const popup = window.open(
                this.renderer.domElement.toDataURL(mime)
              , 'undo3d-logo', 'width=512,height=512,menubar=0,toolbar=0,'
                  + 'location=0,personalbar=0,status=0,resizable=0,scrollbars=0'
            )
        }
    }

}
