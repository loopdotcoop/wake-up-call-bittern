let tally = 0
//// Loads an SVG file and parses its points-data.

//// Matches a line of SVG markup like:
//// <polygon fill="#63ACE5" points="238.9,305.8 199.70,235.1 135,312.062 "/>
const polygonRx = /^<polygon\s+fill="(#[0-9A-F]{6})"\s+points="([.,\s0-9]*)"/

//// Matches a line of SVG markup like:
//// <rect x="194" y="230" fill="#FF0000" width="12.5" height="12.5"/>
const rectRx = /^<rect\s+x="([.0-9]*)"\s+y="([.0-9]*)"\s+fill="(#[0-9A-F]{6})"\s+width="([.0-9]*)"\s+height="([.0-9]*)"/

class Point {
    constructor (x, y, z=0) {
        this.x = x
        this.y = -y // THREE.js’s Y axis is opposite to SVG’s
        this.z = z
    }
}

class Rect {
    constructor (rawRect) {
        const match = rawRect.match(rectRx)

        this.x = match[1]*1
        this.y = match[2]*1
        this.fill = match[3]
        this.width = match[4]*1
        this.height = match[5]*1
    }
}

class Triangle {
    constructor (rawPolygon, svg) {
        const { rects } = svg

        //// Parse the 'fill' and 'points' attributes.
        const match = rawPolygon.match(polygonRx)
        this.fill = match[1]
        const xyCoords = match[2].split(' ').filter( text => text.trim() )
        if (3 !== xyCoords.length)
            throw Error(`Found a polygon with ${xyCoords.length} xy-coords`)

        //// Convert raw SVG points to THREE.js points, and use any rectangles
        //// to add z-depth.
        this.points = []
        for (const xyCoord of xyCoords) {
            const [ x, y ] = xyCoord.split(',')
            if ( x != x*1 || y != y*1 )
                throw Error(`Found a point with xy-coords "${x}, ${y}"`)
            let z = 0
            for (const rect of rects)
                if (x > rect.x && x < rect.x+rect.width
                 && y > rect.y && y < rect.y+rect.height)
                    z = rect.width * 3
            this.points.push( new Point(x, y, z) )
        }

        //// Begin building the THREE triangle.
        const
            p = this.points
          , va = new THREE.Vector3(p[0].x, p[0].y, p[0].z)
          , vb = new THREE.Vector3(p[1].x, p[1].y, p[1].z)
          , vc = new THREE.Vector3(p[2].x, p[2].y, p[2].z)
          , geometry = new THREE.Geometry()
        geometry.vertices.push(va, vb, vc)

        //// Reorder points so that when they are joined into a triangle, the
        //// first line is longest, and the last line shortest (disregarding z).
        const
            [ {x:ax,y:ay}, {x:bx,y:by}, {x:cx,y:cy} ] = this.points
          , dabx = Math.abs(ax-bx), dacx = Math.abs(ax-cx), dbcx = Math.abs(bx-cx)
          , daby = Math.abs(ay-by), dacy = Math.abs(ay-cy), dbcy = Math.abs(by-cy)
          , dab = Math.sqrt(dabx**2 + daby**2)
          , dac = Math.sqrt(dacx**2 + dacy**2)
          , dbc = Math.sqrt(dbcx**2 + dbcy**2)
          , f = geometry.faces
// console.log(~~ax, ~~ay, ~~bx, ~~by, ~~cx, ~~cy);
// console.log(~~dab, ~~dac, ~~dbc);
        let dpq, dqr, dpr
        if (dab > dac && dab > dbc) // a-b is the longest
            if (dbc > dac) { // a-c is the shortest
                f.push( new THREE.Face3(0, 1, 2) ) // a-b, b-c, c-a
                dpq = dab, dqr = dbc, dpr = dac
            } else { // b-c is the shortest
                f.push( new THREE.Face3(1, 0, 2) ) // b-a, a-c, c-b
                dpq = dab, dqr = dac, dpr = dbc
            }
        else if (dbc > dab && dbc > dac) // b-c is the longest
            if (dab > dac) { // a-c is the shortest
                f.push( new THREE.Face3(2, 1, 0) ) // c-b, b-a, a-c
                dpq = dbc, dqr = dab, dpr = dac
            } else { // a-b is the shortest
                f.push( new THREE.Face3(1, 2, 0) ) // b-c, c-a, a-b
                dpq = dbc, dqr = dac, dpr = dab
            }
        else // a-c is the longest
            if (dab > dbc) { // b-c is the shortest
                f.push( new THREE.Face3(2, 0, 1) ) // c-a, a-b, b-c
                dpq = dac, dqr = dab, dpr = dbc
            } else { // a-b is the shortest
                f.push( new THREE.Face3(0, 2, 1) ) // a-c, c-b, b-a
                dpq = dac, dqr = dbc, dpr = dab
            }

        //// Compute UV mapping. In this diagram:
        //// p is (0,1)
        //// q is (1,1)
        //// r is (ps,1-sr)
        ////
        ////            r
        ////   short  # | ##    medium
        ////        #   |    ##
        ////      #     |       ##
        ////    p ##### s ######## q
        ////           long
        const
            q = Math.acos( // cos(q) = (dqr² - dpr² + dpq²) / (2 × dqr × dpq)
                (dqr**2 - dpr**2 + dpq**2) / (2*dpq*dqr) ) // law of cosines
          , drs = Math.sin(q) * dqr // opp/hyp = sin(angle)
          , dps = Math.sqrt(dpr**2 - drs**2) // pythagoras
        geometry.faceVertexUvs[0] = [ [
            new THREE.Vector2(0, 1)
          , new THREE.Vector2(1, 1)
          , new THREE.Vector2(dps/dpq, 1-drs/dpq)
        ] ]
        geometry.computeVertexNormals()

        this.mesh = new THREE.Mesh(geometry, svg.materials[this.fill])

    }
}


export default class Svg {

    constructor (options) {
        this.materials = options.materials
        this.triangles = []
        this.rects = []
        this.mesh = new THREE.Object3D
    }

    fetch (path) {
        return new Promise( (resolve, reject) => {
            fetch( new Request(path) )
               .then( response => {
                    if (! response.ok) reject( Error(response.statusText) )
                    response.text().then(
                        rawSVG => resolve( this.parse(rawSVG) )
                    )
                })
        })
    }

    parse (rawSVG) {
// console.log(rawSVG);
        const lines = rawSVG.split('\n')
        for (const line of lines)
            if ( rectRx.test(line) )
                this.rects.push( new Rect(line) )
        for (const line of lines)
            if ( polygonRx.test(line) )
                this.triangles.push( new Triangle(line, this) )
        for (const triangle of this.triangles)
            this.mesh.add(triangle.mesh)
    }

}
